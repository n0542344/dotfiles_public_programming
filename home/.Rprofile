# ### ### ### ### ### ### ### ### # --------------------------------------------
# ###  ! manually  created !  ### #
# ###        dotfiles         ### #
# ###       2019-03-16        ### #

# ### ### .Rprofile: configure the behaviour of R ==============================



# ### define .Rprofile for being loaded on each startup
#
# CAVE: may limit interoperability!
#
# file information from http://www.onthelambda.com/2014/09/17/fun-with-rprofile-and-customizing-r-startup/
# as well as http://www.gettinggeneticsdone.com/2013/07/customize-rprofile.html


# ### Don't ask for CRAN mirror every time
options("repos" = c(CRAN = "https://cloud.r-project.org/"))


# ### stringsAsFactors for reading files
# options(stringsAsFactors=FALSE)


# ### reduce the number of entries to be printed
options(max.print       = 100)

# ### define when scientific notation should be used
options(scipen          = 5)

# ### define default text editor
options(editor          = "vim")

# ### use stars to indicate significance
# options(show.signif.stars = FALSE)

# ### use console output if possible
options(menu.graphics   = FALSE)

# ### define how the prompt and the continuation should look like as well as the size
options(prompt          = "» ")
options(continue        = "± ")
options(width           = 80)


# ### generate function for faster quitting
q <- function (save = "no", ...) {
  quit(save = save, ...)
}

# ### enable tab-completion for packagenames when using library() or require()
utils::rc.settings(ipck = TRUE)

# ### add a timestamp to every command used
.First <- function(){
  if(interactive()){
    library(utils)
    timestamp(, prefix = paste("##------ [", getwd(),"] ", sep = ""))
  }
}

# ### save commands in "~/.RHistory"
# CAVE: '$HOME' cannot be used
.Last <- function(){
  if(interactive()){
    hist_file <- Sys.getenv("R_HISTFILE")
    if(hist_file == "") hist_file <- "~/.RHistory"
    savehistory(hist_file)
  }
}

# ### load packages on startup without warnings
# sshhh <-
#   function(a.package){
#     suppressWarnings(suppressPackageStartupMessages(library(a.package,
#                                                             character.only = TRUE
#                                                             )
#                                                     )
#                      )
#   }
#
# auto.loads <-c("dplyr")
#
# if(interactive()){
#   invisible(sapply(auto.loads,
#             sshhh))
# }

# ### create a hidden namespace to store functions in for those to survive a "rm(list=ls())"
.env <- new.env()

# ### defines a simple function to remove any row names a data.frame might have
.env$unrowname <- function(x) {
  rownames(x) <- NULL
  x
}

# ### defines a function to sanely undo a "factor()" call
.env$unfactor <- function(df){
  id <- sapply(df, is.factor)
  df[id] <- lapply(df[id], as.character)
  df
}

attach(.env)


# ### define behaviour for startup and shutdown
# .First() run at the start of every script
# Use to load commonly used packages?
.First <- function() {
  # load libraries here?
       }

# .Last() run at the end of every script
.Last <- function() {
  # save command history here?
}

# message to be displayed if .Rprofile loaded successfully
message("\n*** Successfully loaded .Rprofile at ", date(), " ***\n")


