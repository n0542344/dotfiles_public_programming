" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ base system interaction --------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for vinegar ----------------------------
" """ enhanced usability of netrw (only vimscript)
" source: 'https://github.com/tpope/vim-vinegar'
" plugin has to be started manually
" {{{ vim-vinegar netrw usage: 
" -         hop up to the directory listing and seek to the file you just came from.
" ~         go to $HOME
" .         start in command mode with : and the absolute path of file at the end
" !         start in command mode with :! and the absolute path of file at the end
" y.        copy absolute file path
" ^6 / ^^   switch between buffers (present in base vim)
" }}}


" """ configuration for ctrlp.vim --------------------------
" """ fuzzy search for files within a directory from within vim (only vimscript)
" source:  'https://github.com/ctrlpvim/ctrlp.vim'
" plugin has to be started manually
let g:ctrlp_show_hidden = 1             " let ctrlp show hidden files
" {{{ ctrlp usage:
" :CtrlP or :CtrlP [starting-directory]      to invoke CtrlP in find file mode.
" :CtrlPBuffer or :CtrlPMRU                  to invoke CtrlP in find buffer or find MRU file mode.
" :CtrlPMixed                                to search in Files, Buffers and MRU files at the same time.
" <F5>                                       to purge the cache for the current directory to get new files, remove deleted files and apply new ignore options.
" <c-f> and <c-b>                            to cycle between modes.
" <c-d>                                      to switch to filename only search instead of full path.
" <c-r>                                      to switch to regexp mode.
" <c-j>, <c-k> or the arrow keys             to navigate the result list.
" <c-t> or <c-v>, <c-x>                      to open the selected entry in a new tab or in a new split.
" <c-n>, <c-p>                               to select the next/previous string in the prompt's history.
" <c-y>                                      to create a new file and its parent directories.
" <c-z>                                      to mark/unmark multiple files and <c-o> to open them.
" }}}


" """ configuration for vim-eunuch -------------------------
" """ UNIX shell command interaction from within vim (only vimscript)
" source: 'https://github.com/tpope/vim-eunuch'
" plugin has to be used manually
" {{{ vim-eunuch usage:
" :Delete:          Delete a buffer and the file on disk simultaneously.
" :Unlink:          Like :Delete, but keeps the now empty buffer.
" :Move:            Rename a buffer and the file on disk simultaneously.
" :Rename:          Like :Move, but relative to the current file's containing directory.
" :Chmod:           Change the permissions of the current file.
" :Mkdir:           Create a directory, defaulting to the parent of the current file.
" :Cfind:           Run find and load the results into the quickfix list.
" :Clocate:         Run locate and load the results into the quickfix list.
" :Lfind/:Llocate:  Like above, but use the location list.
" :Wall:            Write every open window. Handy for kicking off tools like guard.
" :SudoWrite:       Write a privileged file with sudo.
" :SudoEdit:        Edit a privileged file with sudo.
" }}}


" """ configuration for oscyank ----------------------------
" """ OSC52 clipboard synchronization (only vimscript)
" source: 'https://github.com/greymd/oscyank.vim'
" plugin has to be used manually
noremap <leader>y :Oscyank<CR>
" {{{ oscyank usage:
" :Oscyank                              " yank into system register using osc52
" }}}


" """ configuration for vim-rooter -------------------------
" """ change root directory to vcs-directory (only vimscript, tests are in bash)
" source: 'https://github.com/airblade/vim-rooter'
let g:rooter_manual_only = 0




" """ """ to delete """ """ --------------------------------

" " """ configuration for nerdtree -------------------------
" " """ file system navigation (alternative to netrw) (only vimscript)
" " source: 'https://github.com/scrooloose/nerdtree'
" " plugin has to be started manually
" " {{{ NERDTree usage:
" " * Press o to open the file in a new buffer or open/close directory.
" " * Press t to open the file in a new tab.
" " * Press i to open the file in a new horizontal split.
" " * Press s to open the file in a new vertical split.
" " * Press I to show/hide hidden files.
" " * Press p to go to parent directory.
" " * Press r to refresh the current directory.
" " {{{ additional NERDTree-options:
" " o........Open files, directories and bookmarks......................|NERDTree-o|
" " go.......Open selected file, but leave cursor in the NERDTree......|NERDTree-go|
" "          Open selected bookmark dir in current NERDTree
" " t........Open selected node/bookmark in a new tab...................|NERDTree-t|
" " T........Same as 't' but keep the focus on the current tab..........|NERDTree-T|
" " i........Open selected file in a split window.......................|NERDTree-i|
" " gi.......Same as i, but leave the cursor on the NERDTree...........|NERDTree-gi|
" " s........Open selected file in a new vsplit.........................|NERDTree-s|
" " gs.......Same as s, but leave the cursor on the NERDTree...........|NERDTree-gs|
" " <CR>.....User-definable custom open action.......................|NERDTree-<CR>|
" " O........Recursively open the selected directory....................|NERDTree-O|
" " x........Close the current nodes parent.............................|NERDTree-x|
" " X........Recursively close all children of the current node.........|NERDTree-X|
" " e........Edit the current dir.......................................|NERDTree-e|
" "
" " double-click....same as |NERDTree-o|.
" " middle-click....same as |NERDTree-i| for files, and |NERDTree-e| for dirs.
" "
" " D........Delete the current bookmark ...............................|NERDTree-D|
" "
" " P........Jump to the root node......................................|NERDTree-P|
" " p........Jump to current nodes parent...............................|NERDTree-p|
" " K........Jump up inside directories at the current tree depth.......|NERDTree-K|
" " J........Jump down inside directories at the current tree depth.....|NERDTree-J|
" " <C-J>....Jump down to next sibling of the current directory.......|NERDTree-C-J|
" " <C-K>....Jump up to previous sibling of the current directory.....|NERDTree-C-K|
" "
" " C........Change the tree root to the selected dir...................|NERDTree-C|
" " u........Move the tree root up one directory........................|NERDTree-u|
" " U........Same as 'u' except the old root node is left open..........|NERDTree-U|
" " r........Recursively refresh the current directory..................|NERDTree-r|
" " R........Recursively refresh the current root.......................|NERDTree-R|
" " m........Display the NERDTree menu..................................|NERDTree-m|
" " cd.......Change the CWD to the dir of the selected node............|NERDTree-cd|
" " CD.......Change tree root to the CWD...............................|NERDTree-CD|
" "
" " I........Toggle whether hidden files displayed......................|NERDTree-I|
" " f........Toggle whether the file filters are used...................|NERDTree-f|
" " F........Toggle whether files are displayed.........................|NERDTree-F|
" " B........Toggle whether the bookmark table is displayed.............|NERDTree-B|
" "
" " q........Close the NERDTree window..................................|NERDTree-q|
" " A........Zoom (maximize/minimize) the NERDTree window...............|NERDTree-A|
" " ?........Toggle the display of the quick help.......................|NERDTree-?|
" " }}}
" " }}}
" " CAVE: not really needed

