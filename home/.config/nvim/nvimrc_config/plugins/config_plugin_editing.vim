" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ editing ------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for vim-rsi ----------------------------
" """ add readline keybindings for vim
" source: 'https://github.com/tpope/vim-rsi'
" plugin loads automatically


" """ configuration for undotree ---------------------------
" """ create an undo-tree structure (not saved on disk) (only vimscript)
" source: 'https://github.com/mbbill/undotree'
" plugin needs to be used actively
" {{{ undotree usage:
" :UndotreeToggle               " toggle the undotree
" }}}


" """ configuration for vim-surround -----------------------
" """ change the surroundings (brackets, quotes, ...) (only vimscript)
" source: 'https://github.com/tpope/vim-surround'
" plugin needs to be used actively
" {{{ vim-surround usage:
" add(y)/change(c)/delete(d) surroundings (brackets, quotes, tags, ...)
" e.g. ysiw] ,  cs]} ,  ds}
" ysiw]   add surroundings in word as ]
" cs]}    change surroundings from ] to }
" ds}     delete surrounding }
" v S"    add surroundings around a visual selection
" }}}


" """ configuration for vim-repeat -------------------------
" """ repeat actions on more complex operations (only vimscript)
" source: 'https://github.com/tpope/vim-repeat'


" """ configuration for targets.vim ------------------------
" """ define additional text objects (only vimscript (and make for tests))
" source: 'https://github.com/wellle/targets.vim'
" plugin needs to be used actively
" {{{ targets.vim usage:
" da,     delete around the comma
" cin)    change in the next parenthesis
" }}}


" """ configuration for auto-pairs -------------------------
" """ automatic creation of paired symbols (brackts, quotes, ...) (only vimscript)
" source: 'https://github.com/jiangmiao/auto-pairs'
" plugin starts automatically


" """ configuration for vim-unimpaired ---------------------
" """ add mnemonics for often used commands (jumping, adding lines, ...) (only vimscript)
" source: 'https://github.com/tpope/vim-unimpaired'
" plugin needs to be used actively
" {{{ vim-unimpaired usage:
" [ / ] b           go to previous / next buffer (:bprev / :bnext)
" [ / ] t           go to previous / next tab (:tprev / :tnext)
" [ / ] <space>     add line above / below
" [ / ] f           open the previous / next file in the current folder
" [ / ] e           exchange the current line with the previous / next line
" yob               toggle background (light/dark)
" yon / yor         toggle number / relativenumber
" yos               toggle spelling
" }}}


" """ configuration for vim-easymotion ---------------------
" """ add a more powerfull search mode (only vimscript)
" source: 'https://github.com/easymotion/vim-easymotion'
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
" s{char}{char} to move to {char}{char}
"nmap s <Plug>(easymotion-overwin-f2)
" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)


" """ configuration for vim-visual-star-search -------------
" """ use '*' and '#' when visually selecting (only vimscript)
" source: 'https://github.com/bronson/vim-visual-star-search'
" plugin needs to be used actively


" """ configuration for vim-swap ---------------------------
" """ swap parts of a line (only vimscript (and sh for tests))
" source: 'https://github.com/machakann/vim-swap'
" plugin needs to be used actively
" {{{ vim-swap usage:
" g<                swap argument with the one before
" g>                swap argument with the one after
" gs?               swap argument interactively
"   h / l           swap items
"   j / k           choose items
"   1 .. 9          select the 1st .. 9th item
"   g / G           group / unGroup items
"   s / S / r       sort / reverse items
" }}}


" """ configuration for vim-commentary ---------------------
" """ automatic creation of comments depending on file type (only vimscript)
" source: 'https://github.com/tpope/vim-commentary'
" plugin has to be used actively
" {{{ vim-commentary usage:
" 'gcc':        comment line
" 'gc<motion>': comment lines around motion (e.g. 'gcap' - comment around paragraph)
" }}}
" update config: autocmd FileType r setlocal commentstring=#\ %s


" """ configuration for vim-abolish ------------------------
" """ coerce variable names (only vimscript)
" source: 'https://github.com/tpope/vim-abolish'
" {{{ vim-abolish usage:
" crs               coerce to snake_case
" crm               coerce to MixedCase 
" crc               coerce to camelCase 
" crs               coerce to snake_case
" cru               coerce to UPPER_CASE 
" cr-               coerce to dash-case
" cr.               coerce to dot.case 
" cr<space>         coerce to space case 
" crt               coerce to Title Case 
" }}}


" """ configuration for VimCompletesMe ---------------------
" """ improved completion features for vim (only vimscript)
" source: 'https://github.com/ajh17/VimCompletesMe'
" {{{ VomCompletesMe usage:
" Without any configuration, the Tab key will, depending on the context, offer:
"   Vim's local keyword completion (Ctrl-X_Ctrl-N)
"   File path completion when typing a path (Ctrl-X_Ctrl-F)
"   Omni-completion after typing a character specified by g:vcm_omni_pattern (Ctrl-X_Ctrl-O)
" With a b:vcm_tab_complete variable, you can set the Tab key to use the following type of completions:
"   Dictionary words (Ctrl-X_Ctrl-K)
"   User-defined completion (Ctrl-X_Ctrl-U)
"   Tags (Ctrl-X_Ctrl-])
"   Vim command line (Ctrl-X_Ctrl-V)
"   Omni completion (Ctrl-X_Ctrl-O)
" }}}


" """ configuration for vim-speeddating --------------------
" """ add option to work with dates in vim (only vimscript)
" source: 'https://github.com/tpope/vim-speeddating'
" {{{ vim-speeddating usage:
" <C-A>                         Increment by [count] the component under the cursor.
" <C-X>                         Decrement by [count] the component under the cursor.
" d<C-A>                        Change the time under the cursor to the current time in UTC.
" d<C-X>                        Change the time under the cursor to the current local time
" }}}


" CAVE: 'quickfix-reflector.vim' and 'quickr-preview.vim' are conflicting!
" """ configuration for quickfix-reflector -----------------
" """ perform changes in the quickfix buffer (only vimscript, testing in ruby)
" source: 'https://github.com/stefandtw/quickfix-reflector.vim'
" plugin automatically enabled
let g:qf_join_changes = 1
let g:qf_write_changes = 0


" """ configuration for quickr-preview ---------------------
" """ enable quickfix buffer preview (only vimscript)
" source: 'https://github.com/ronakg/quickr-preview.vim'
" {{{ vim-qf-preview usage:
" <leader><space>               Preview the quickfix/location result in a preview window
" <enter>                       Open the quickfix/location result in a new buffer like usual
" }}}
let g:quickr_preview_position = 'right'
let g:quickr_preview_options = 'number norelativenumber nofoldenable'
let g:quickr_preview_on_cursor = 1
let g:quickr_preview_exit_on_enter = 1


" """ configuration for vim-bufkill ------------------------
" """ add option to kill buffers without changing the split
" source: 'https://github.com/qpkorr/vim-bufkill'
" plugin has to be used actively
" {{{ vim-bufkill usage:
" Unloading/Deleting/Wiping (The key mappings are the uppercase version of the :bun :bd :bw Vim commands)
"   :BUN            When you wish to unload a file from the buffer and keep the window/split intact
"   :BD             When you wish to delete a file from the buffer and keep the window/split intact
"   :BW             When you wish to wipe a file from the buffer and keep the window/split intact
" Moving through buffers:
"   :BB             To move backwards through recently accessed buffers
"   :BF             To move forwards
"   :BA             To move to an alternate buffer and keep the cursor in the same column
" }}}



" """ """ TO DELETE """ """ ----------------------------------------------------

" """ configuration for vim-multiple-cursors --------------- DEPRECATED
" """ work with multiple cursors (only vimscript, but tests are done using ruby)
" source: 'https://github.com/terryma/vim-multiple-cursors'
" plugin needs to be used actively
" {{{ vim-multiple-cursers usage:
" <C-n>:  start      - start multicursor and add a virtual cursor + selection on the match
" <C-n>:  next       - add a new virtual cursor + selection on the next match
" <C-x>:  skip       - skip the next match
" <C-p>:  prev       - remove current virtual cursor + selection and go back on previous match
" <A-n>:  select all - start multicursor and directly select all matches
" }}}


