" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ viewing ------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for NrrwRgn ----------------------------
" """ create a narrow region of text (only vimscript (and perl/make/shell for tests)
" source: 'https://github.com/chrisbra/NrrwRgn'
" plugin has to be used manually
" {{{ NrrwRgn usage: 
" <Leader>nr		 - Open the current visual selection in a new narrowed window
" NR:   open selection in a new window
" NW:   open current visual window in a new window
" NRP:  mark a selection so you can open multiple parts of a file
" NRM:  after using NRP, run this so you can open those parts!
" }}}


" """ configuration for tabular ----------------------------
" """ make table-like indentations (only vimscript)
" source: 'https://github.com/godlygeek/tabular'
" plugin has to be used manually
" {{{ tabular usage:
" ':Tabularize /,'  to align on the ','
" }}}
" additional information on http://vimcasts.org/episodes/aligning-text-with-tabular-vim/
"let g:tabular_loaded = 1                " use this command to disable the 'Tabularize'-plugin


" """ configuration for vim-lion ---------------------------
" """ make table-like indentations (only vimscript)
" source: 'https://github.com/tommcdo/vim-lion'
" plugin has to be used manually
" {{{ vim-lion usage:
" gl / gL           perform space to the left / right of the allignment character
" glip=             align using '=' in paragraph
" }}}

" """ configuration for csv.vim ----------------------------
" """ working with csv-files (only vimscript (and make for the install))
" source: 'https://github.com/chrisbra/csv.vim'
" plugin has to be used manually
" {{{ csv.vim usage:
" ':CSVHeader' to always display the header in a separate split
" ':CSVTabularize' or ':CSVTable' to display the data as formatted/unformatted table
" see also https://github.com/chrisbra/csv.vim
" }}}


" """ configuration for goyo.vim ---------------------------
" """ distraction-free writing in Vim (only vimscript)
" source: 'https://github.com/junegunn/goyo.vim'
" plugin has to be used manually
" {{{ Goyo usage:
" :Goyo                 Toggle Goyo
" :Goyo [dimension]     Turn on or resize Goyo
" :Goyo!                Turn Goyo off
" }}}


" """ configuration for limelight.vim ----------------------
" """ distraction-free surrounding in Vim (only vimscript)
" source: 'https://github.com/junegunn/limelight.vim'
" plugin has to be used manually
" {{{ Limelight usage:
" :Limelight [0.0 ~ 1.0]        " Turn Limelight on
" :Limelight!                   " Turn Limelight off
" :Limelight!! [0.0 ~ 1.0]      " Toggle Limelight
" }}}
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'
nnoremap <leader>gl     :Goyo <CR> :Limelight!! <CR>


" """ configuration for scratch ----------------------------
" """ create a temporary scratch buffer (only vimscript)
" source: 'https://github.com/mtth/scratch.vim'
" plugin has to be used manually
" {{{ scratch usage:
" :Scratch          open scratch buffer
" gs / gC           open scratch buffer (changed key bindings)
" }}}
let g:scratch_insert_autohide = 0
nmap <leader>gc <plug>(scratch-insert-reuse)
nmap <leader>gC <plug>(scratch-insert-clear)
xmap <leader>gc <plug>(scratch-selection-reuse)
xmap <leader>gC <plug>(scratch-selection-clear)


