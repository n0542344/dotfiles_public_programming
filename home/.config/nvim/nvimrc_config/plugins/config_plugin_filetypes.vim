" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ filetypes ----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ add orgmode-features when working with org-files (not only vimscript, also python)
" source: 'https://github.com/jceb/vim-orgmode'
" {{{ vim-orgmode usage:
" m}                            " move heading down
" m{                            " move heading up
" m]]                           " move subtree down
" m[[                           " move subtree up
" gn                            " next link
" go                            " previous link
" <localleader>cc               " toggle status
" <localleader>cn or <CR>       " insert checkbox below
" <localleader>cN or <C-S-CR>   " insert checkbox above
" <localleader>st               " set tags
" <localleader>sa               " insert date
" <localleader>si               " insert inactive date
" <localleader>pa               " insert date by using calendar selection
" <localleader>pi               " insert inactive date by using calendar selection
" <localleader>ep               " export as PDF
" <localleader>eb               " export as Beamer PDF
" <localleader>eh               " export as HTML
" <localleader>el               " export as LaTeX
"   formating:
"                               " *bold*
"                               " /italic/
"                               " _underline_
"                               " +strike-through+
"                               " =code=
"                               " ~verbatim~
" }}}

