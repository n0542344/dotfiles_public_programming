" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  x0ff manually created  """ "
" """        dotfiles         """ "
" """       2020-10-11        """ "

" """ """ .config/nvim/plugins.vim (non-standard, need trivial dependencies) ===




" Load packager with additional dependencies (also only when you need it)
function! PackagerInitWithTrivialDependencies() abort
  packadd vim-packager
  call packager#init()
  call packager#add('kristijanhusak/vim-packager', { 'type': 'opt' })



" """ version control ----------------------------------------------------------

" git-integration using fugitive
  call packager#add('https://github.com/tpope/vim-fugitive')

" add a 'tig'-like browser to fugitive
  call packager#add('https://github.com/junegunn/gv.vim')

" git-integration using chunk-wise display of changes
  call packager#add('https://github.com/mhinz/vim-signify')

" git-integration using chunk-wise display of changes
  call packager#add('https://github.com/airblade/vim-gitgutter')
" CAVE: not really needed



" """ tags ---------------------------------------------------------------------

" continuous generation of tags
  call packager#add('https://github.com/ludovicchabant/vim-gutentags')



" """ formatter ----------------------------------------------------------------

" automatically format code (only vimscript, but needs formatters)
  call packager#add('https://github.com/sbdchd/neoformat')

" shell formatter for neoformat
"  call packager#add('https://github.com/z0mbix/vim-shfmt')
" CAVE: dependency (shfmt) not available in Debian standard repository



endfunction


command!        PackagerInstallWithTrivialDependencies call    PackagerInitWithTrivialDependencies() | call packager#install()
command! -bang  PackagerUpdateWithTrivialDependencies  call    PackagerInitWithTrivialDependencies() | call packager#update({ 'force_hooks': '<bang>' })
command!        PackagerCleanWithTrivialDependencies   call    PackagerInitWithTrivialDependencies() | call packager#clean()
command!        PackagerStatusWithTrivialDependencies  call    PackagerInitWithTrivialDependencies() | call packager#status()





" """ """ ----------------------------------------------------------------------
" """ version control ----------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_version-control.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_version-control.vim
endif


" """ configuration for vim-fugitive -----------------------
" """ configuration for gv ---------------------------------
" """ configuration for vim-signify ------------------------
" """ configuration for vim-gitgutter ----------------------



" """ """ ----------------------------------------------------------------------
" """ tags ---------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_tags.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_tags.vim
endif



" """ configuration of vim-gutentags -----------------------



" """ """ ----------------------------------------------------------------------
" """ formatter ----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_formatter.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_formatter.vim
endif



" """ configuration for neoformat --------------------------
" """ configuration for vim-shfmt --------------------------

