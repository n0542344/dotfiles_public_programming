" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  x0ff manually created  """ "
" """        dotfiles         """ "
" """       2020-10-11        """ "

" """ """ .config/nvim/plugins.vim (non-standard, need trivial dependencies) ===




" Load packager with additional dependencies (also only when you need it)
function! PackagerInitWithComplexDependencies() abort
  packadd vim-packager
  call packager#init()
  call packager#add('kristijanhusak/vim-packager', { 'type': 'opt' })


" """ document export ----------------------------------------------------------

" compile TeX-files
  call packager#add('https://github.com/vim-scripts/TeX-PDF')


" use pandoc from within vim (vimscript and python)
  call packager#add('https://github.com/vim-pandoc/vim-pandoc')

" use pandoc for syntax support (vimscript and vim-pandoc plugin)
  call packager#add('https://github.com/vim-pandoc/vim-pandoc-syntax')

" " use LaTeX from within vim (vimscript, python and neovim-remote)
"   call packager#add('https://github.com/lervag/vimtex')



" """ Nvim-R -------------------------------------------------------------------

" interaction between vim, R and tmux (vimscript, R, python, c)
  call packager#add('https://github.com/jalvesaq/Nvim-R')

" R-related runtimes for vim (vimscript and R)
  call packager#add('https://github.com/jalvesaq/R-Vim-runtime')

" interacting between R, pandoc, Rmarkdown and vim (vimscript and R)
"  call packager#add('https://github.com/vim-pandoc/vim-rmarkdown')
" CAVE: this plugin interferes with the chunk-detection of Nvim-R!



" """ filetypes ----------------------------------------------------------------

" add orgmode-features when working with org-files (python and vimscript)
  call packager#add('https://github.com/jceb/vim-orgmode')



" """ fuzzy finder -------------------------------------------------------------

" use fzf fuzzy finder for navigation (go, ruby, shell and vimscript)
  "call packager#add('https://github.com/junegunn/fzf', { 'do': { -> fzf#install() } })
  call packager#add('https://github.com/junegunn/fzf', { 'do': { -> fzf#install() } })
  call packager#add('https://github.com/junegunn/fzf.vim')







endfunction


command!        PackagerInstallWithComplexDependencies call    PackagerInitWithComplexDependencies() | call packager#install()
command! -bang  PackagerUpdateWithComplexDependencies  call    PackagerInitWithComplexDependencies() | call packager#update({ 'force_hooks': '<bang>' })
command!        PackagerCleanWithComplexDependencies   call    PackagerInitWithComplexDependencies() | call packager#clean()
command!        PackagerStatusWithComplexDependencies  call    PackagerInitWithComplexDependencies() | call packager#status()



" """ """ ----------------------------------------------------------------------
" """ document export ----------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_document-export.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_document-export.vim
endif


" """ configuration of TeX-PDF -----------------------------
" """ configuration for vim-pandoc -------------------------
" """ configuration for vim-pandoc-syntax ------------------
" " """ configuration for vimtex -----------------------------




" """ """ ----------------------------------------------------------------------
" """ Nvim-R -------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_Nvim-R.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_Nvim-R.vim
endif


" """ configuration for Nvim-R -----------------------------
" """ configuration ov R-Vim-runtime -----------------------
" """ configuration of vim-rmarkdown -----------------------



" """ """ ----------------------------------------------------------------------
" """ filetypes ----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_filetypes.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_filetypes.vim
endif

" """ configuration for vim-orgmode ------------------------



" """ """ ----------------------------------------------------------------------
" """ fuzzy finder -------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_fuzzy-finder.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_fuzzy-finder.vim
endif



" """ configuration for fzf.vim ----------------------------


