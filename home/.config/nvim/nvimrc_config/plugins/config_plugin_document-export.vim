" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ document export ----------------------------------------------------------
" """ """ ----------------------------------------------------------------------



" """ configuration of TeX-PDF -----------------------------
" """ compile TeX-files (only vimscript)
" source: 'https://github.com/vim-scripts/TeX-PDF'


" """ alternative LaTeX-workaround (without need for plugins)
" LaTeX (rubber) macro for compiling
nnoremap <leader>c :w<CR>:!rubber --pdf --warn all %<CR>
" View PDF macro; '%:r' is current file's root (base) name.
nnoremap <leader>v :!zathura %:r.pdf &<CR><CR>










" """ configuration for vim-pandoc -------------------------
" """ use pandoc for document conversion (not only vimscript, also python and external dependency - pandoc)
" source:  'https://github.com/vim-pandoc/vim-pandoc'
" more information: 'https://github.com/connermcd/dotfiles/blob/master/.vimrc'
" plugin has to be used manually
" {{{ vim-pandoc usage:
" <localleader>i                    when selected, toggles emphasis [vn]
" <localleader>b                    when selected, toggles bold [vn]
" <localleader>`                    when selected, toggles verbatim [vn]
" <localleader>~~                   when selected, toggles strikeout [vn]
" <localleader>^                    when selected, toggles superscript [vn]
" <localleader>_                    when selected, toggles subscript [vn]
" :Pandoc! beamer                   convert md-file into LaTeX beamer presentation and automatically open it
" :Pandoc slidy --self-contained    convert file into Slidy presentation including (almost) all dependencies
" }}}
" automatically execute commands and open automatically on write (and select, which one to open)
"let g:pandoc#command#autoexec_on_writes = 1
"let g:pandoc#command#autoexec_command = 'Pandoc! beamer'
"let g:pandoc#command#prefer_pdf = 1
" activate bibliography tools (use Ctrl-X Ctrl-O to activate)
let g:pandoc#biblio#use_bibtool = 1
let g:pandoc#completion#bib#mode = 'citeproc'
" select what file types should be handeled using pandoc
" default: "pandoc", "rst", "textile"
" added: "markdown" (using the option #pandoc_markdown)
let g:pandoc#filetypes#pandoc_markdown = 1
let g:pandoc#filetypes#handled = ["pandoc", "rst", "textile", "markdown"]
" stop pandoc from overwriting the 'foldcolumn'-value
let g:pandoc#folding#fdc = 4
" don't remap k and l to gk and gl
let g:pandoc#keyboard#display_motions = 0



" """ configuration for vim-pandoc-syntax ------------------
" """ use pandoc for syntax support (not only vimscript, also python and external dependency - pandoc and pandoc-plugin)
" source: 'https://github.com/vim-pandoc/vim-pandoc-syntax'
" plugin starts automatically


" " """ configuration for vimtex -----------------------------
" " """ use LaTeX (not only vimscript, also python and neovim-remote)
" " source: 'https://github.com/lervag/vimtex'
" " plugin has to be used manually
" " {{{ vimtex usage:
" " mark text-objects when in visual mode:
" "   i? / a? mark in / around something
" "   ?d      mark in / around delimiter (e.g. parenthesis)
" "   ?c      mark in / around commands (e.g. \textit{something})
" "   ?e      mark in / around environments (e.g. \begin{something} and \end{something})
" "   ?$      mark in/around inline math structure
" " work with commands and/or environments
" "   dsc|csc delete/change the surrounding command
" "   dse|cse delete/change the surrounding environment
" " motions and mappings:
" "   ]m / [M go to next environment begin / previous environment end
" "   % ...   move between matching delimeters
" " :VimtexTocOpen|:VimtexTocToggle: open a clickable toc in the left pane (q will close the window)
" " :VimtexInfo: prints basic information
" " :VimtexCountWords|:VimtexCountLetters: count the number of words/letters in the document. 
" " :VimtexLabelsOpen|:VimtexLabelsToggle: open table of labels.
" " :VimtexCompileOutput: show the output form the compilation command (i.e. from latexmk)
" " :VimtexErrors: open quickfix window if there are errors or warnings.
" " :VimtexClean: clean auxilliary files like *.aux, *.out, and so on files. Use :VimtexClean! to remove everything, including the generated pfd file.
" "
" " REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
" "filetype plugin on                     " already used in other config-files
" 
" " IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" " can be called correctly.
" "set shellslash                         " not needed (since no win32-os)
" 
" " OPTIONAL: This enables automatic indentation as you type.
" "filetype indent on                     " already used in other config-files
" 
" " OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" " 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" " }}}
" " The following changes the default filetype back to 'tex':
" let g:tex_flavor='latex'
" " set compiler for vimtex
" let g:vimtex_compiler_progname = 'nvr'
" " neovim-remote (https://github.com/mhinz/neovim-remote) has to be installed
" " for this to work (using pip3 install neovim-remote)
" " enable completion
" let g:vimtex_complete_enabeled = 1
" " set viewer for vimtex
" let g:vimtex_view_method = 'zathura'
" " add additional text objects (if targets.vim is available)
" let g:vimtex_text_obj_variant = 'auto'



