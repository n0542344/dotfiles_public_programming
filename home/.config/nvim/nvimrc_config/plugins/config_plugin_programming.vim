" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ programming --------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration of vim-slime ---------------------------
" """ interaction between vim, python/shell/other REPL languages and tmux (only vimscript)
" source: 'https://github.com/jpalardy/vim-slime'
let g:slime_target = "screen"



" """ configuration of repl.nvim ---------------------------
" """ interaction between vim, python/shell/other REPL languages and tmux (only vimscript)
" source: 'https://gitlab.com/HiPhish/repl.nvim'

let repl_R = {
    \ 'bin': 'R',
    \ 'args': [],
    \ 'syntax': 'r',
    \ 'title': 'R REPL',
    \ }

" define automatic command to be run when opening an R-script
autocmd BufNewFile,BufRead  *.R,*.Rmd   call repl#define_repl('r', repl_R, 'keep')
autocmd FileType            r           call repl#define_repl('r', repl_R, 'keep')

let repl_perl = {
    \ 'bin': '/usr/bin/perl',
    \ 'args': [],
    \ 'syntax': 'perl',
    \ 'title': 'Perl REPL',
    \ }

autocmd BufNewFile,BufRead  *.pl        call repl#define_repl('perl', repl_perl, 'keep')
autocmd FileType            perl        call repl#define_repl('perl', repl_perl, 'keep')

vmap <leader>ds  <Plug>(ReplSend)
nmap <leader>ds  <Plug>(ReplSend)
nmap <leader>dss <Plug>(ReplSendLine)
nmap <leader>ds_ <Plug>(ReplSendLine)



" """ configuration of vimcmdline --------------------------
" """ interaction between vim, python/shell/other REPL languages and tmux (only vimscript)
" source: 'https://github.com/jalvesaq/vimcmdline'
" additional information: 'https://girke.bioinformatics.ucr.edu/GEN242/mydoc_tutorial_02.html'
" plugin has to be used manually
" {{{ vimcmdline usage:
" <leader>s         start console
" <space>           send current line to console
" <leader><space>   send current line to console and stay on current position
" <leader>p         send current paragraph to console
" <leader>f         send current file to console
" <leader>q         quit console
" }}}
" start console split vertically
"let cmdline_vsplit = 1
"let cmdline_term_width = 80
" send <Esc> to the console (press <C-\><C-N> to go from Terminal mode to Normal mode)
"let cmdline_esc_term = 0
" remap <leader>s to <leader>; (otherwise it interferes with vim-jedi)
"let cmdline_map_start          = '<LocalLeader>;'




" """ configuration of neoterm -----------------------------
" """ interaction between vim, python/shell/other REPL languages and tmux (only vimscript)
" source: 'https://github.com/kassio/neoterm'
nmap <leader>tt  <Plug>(neoterm-repl-send)
nmap <leader>tl  <Plug>(neoterm-repl-send-line)
vmap <leader>ts  :TREPLSendSelection<CR>





" """ configuration for codi -------------------------------
" """ automatic REPL for files (only vimscript)
" source: 'https://github.com/metakirby5/codi.vim'
let g:codi#autoclose = 1
let g:codi#interpreters = {
  \ 'python': {
      \ 'bin': 'python',
      \ 'prompt': '^\(>>>\|\.\.\.\) ',
      \ },
  \ 'r': {
      \ 'bin': 'R',
      \ 'prompt': '^> ',
      \ },
  \ }


