" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ snippets integration -----------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration of quicktex ----------------------------
" """ write LaTeX-commands quickly (only vimscript)
" source: 'https://github.com/brennier/quicktex'
let g:quicktex_tex = {
    \' '       : "\<ESC>:call search('<+.*+>')\<CR>\"_c/+>/e\<CR>",
    \'m'       : '\( <+++> \) <++>',
    \'italic'  : '\\italic{<+++>} <++>',
    \'textbf'  : '\\textbf{<+++>} <++>',
    \'frame'   : "\\begin{frame}\<CR><+++>\<CR>\\end{frame}",
    \'itemize' : "\\begin{itemize}\<CR><+++>\<CR>\\end{itemize}",
    \'itm'     : '\item ',
\}





" """ configuration of vim-snipmate ------------------------
" """ snippets manager for vim (only vimscript (and sh for tests))
" source: 'https://github.com/garbas/vim-snipmate'
" {{{ vim-snipmate usage:
"   <snippet-abreviation><Tab>  expand snippet
"   <Tab>                       advance to the next field within snippet
"   <Ctrl-R><Tab>               show all available snippets
" }}}
" snippets must be in the runtimepath for vim-snipmate to work
set rtp+=~/.config/nvim/pack/minpac/start/vim-snippets/snippets/







" """ configuration of tlib_vim ----------------------------
" """ utility functions for vim (dependency of vim-snipmate) (vimscript, some ruby (?))
" source: 'https://github.com/tomtom/tlib_vim'


" """ configuration of vim-addon-mw-utils ------------------
" """ interpret a file by function and cache file automatically (dependency of vim-snipmate) (only vimscript)
" source: 'https://github.com/marcweber/vim-addon-mw-utils'


" """ configuration of vim-snippets ------------------------
" """ snippets for vim-snipmate/UtilSnips (only vimscript)
" source: 'https://github.com/honza/vim-snippets'







" " """ configuration of UltiSnips ---------------------------
" " """ UltiSnips completion framework (not only vimscript, also python)
" " source: 'https://github.com/sirver/UltiSnips'
" " more mature alternative to vim-snipmate (but needs python)
" " can interact with ncm2
" autocmd FileType r      UltiSnipsAddFiletypes r
" autocmd FileType py     UltiSnipsAddFiletypes python
" autocmd FileType sh     UltiSnipsAddFiletypes sh
" autocmd FileType bash   UltiSnipsAddFiletypes bash
"
" " Interaction between UltiSnips and NCM/ncm2
"
" " Press enter key to trigger snippet expansion
" " The parameters are the same as `:help feedkeys()`
" "inoremap <silent> <expr> <CR> ncm2_ultisnips#expand_or("\<CR>", 'n')
"
" " c-j c-k for moving in snippet
" let g:UltiSnipsExpandTrigger        = "<Plug>(ultisnips_expand)"
" let g:UltiSnipsJumpForwardTrigger = "<c-j>"
" let g:UltiSnipsJumpBackwardTrigger    = "<c-k>"
" let g:UltiSnipsRemoveSelectModeMappings = 1

" " old function
" let g:UltiSnipsExpandTrigger = "<Plug>(ultisnips_expand_or_jump)"
" let g:UltiSnipsJumpForwardTrigger = "<Plug>(ultisnips_expand_or_jump)"
"
" function! UltiSnipsExpandOrJumpOrTab()
"   call UltiSnips#ExpandSnippetOrJump()
"   if g:ulti_expand_or_jump_res > 0
"     return ""
"   else
"     return "\<Tab>"
"   endif
" endfunction
"
" inoremap <silent> <expr> <Tab>
"       \ ncm2_ultisnips#expand_or("\<Plug>(ultisnips_try_expand)")
"
" inoremap <silent> <Plug>(ultisnips_try_expand)
"       \ <C-R>=UltiSnipsExpandOrJumpOrTab()<CR>
"
" snoremap <silent> <Tab>
"       \ <Esc>:call UltiSnips#ExpandSnippetOrJump()<cr>




" " """ VSCode snippets (using LSP, currently disabeled, since not working)
"
"
" " """ configuration of vim-snips ---------------------------
" " """ snippets plugin --------------------------------------
" " source: 'https://github.com/hrsh7th/vim-vsnip'
"
"
" " """ configuration of vim-snips-integ ---------------------
" " """ integration for snippets plugin to lsp ---------------
" " source: 'https://github.com/hrsh7th/vim-vsnip-integ'


