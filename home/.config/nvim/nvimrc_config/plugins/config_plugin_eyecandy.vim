" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ eyecandy -----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for lightline.vim ----------------------
" """ enable a colourful vim status line (only vimscript)
" source: 'https://github.com/itchyny/lightline.vim'
" plugin starts automatically
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left':   [ [ 'mode', 'paste'  ],
      \               [ 'bufnum', 'filename', 'modified', 'readonly' ] ],
      \   'right':  [ [ 'lineinfo' ],
      \               [ 'percent' ],
      \               [ 'fileformat', 'fileencoding', 'filetype' ] ]
      \         },
      \ 'inactive': {
      \   'left':   [ [ 'bufnum', 'filename' ] ],
      \   'right':  [ [ 'lineinfo'  ],
      \               [ 'percent' ] ]
      \         }
      \     }


" """ configuration for vim-highlightedyank ----------------
" """ highlight yanked areals (only vimscript, tests in sh)
" source: 'https://github.com/machakann/vim-highlightedyank'
" plugin starts automatically


" """ configuration for vim-signature ----------------------
" """ highlight marks (only vimscript)
" source: 'https://github.com/kshenoy/vim-signature'
" plugin starts automatically
" {{{ vim-signature usage:
" mx           Toggle mark 'x' and display it in the leftmost column
" dmx          Remove mark 'x' where x is a-zA-Z

" m,           Place the next available mark
" m.           If no mark on line, place the next available mark. Otherwise, remove (first) existing mark.
" m-           Delete all marks from the current line
" m<Space>     Delete all marks from the current buffer
" ]`           Jump to next mark
" [`           Jump to prev mark
" ]'           Jump to start of next line containing a mark
" ['           Jump to start of prev line containing a mark
" `]           Jump by alphabetical order to next mark
" `[           Jump by alphabetical order to prev mark
" ']           Jump by alphabetical order to start of next line having a mark
" '[           Jump by alphabetical order to start of prev line having a mark
" m/           Open location list and display marks from current buffer

" m[0-9]       Toggle the corresponding marker !@#$%^&*()
" m<S-[0-9]>   Remove all markers of the same type
" ]-           Jump to next line having a marker of the same type
" [-           Jump to prev line having a marker of the same type
" ]=           Jump to next line having a marker of any type
" [=           Jump to prev line having a marker of any type
" m?           Open location list and display markers from current buffer
" m<BS>        Remove all markers
" }}}


" """ configuration for rainbow ----------------------------
" """ color parentheses using different colors per level (only vimscript)
" source: 'https://github.com/frazrepo/vim-rainbow'
" plugin has to be started manually
let g:rainbow_active = 1


" """ configuration for ShowTrailingWhitespace -------------
" """ color trailing whitespace (only vimscript)
" source: 'https://github.com/inkarkat/vim-ShowTrailingWhitespace'
" plugin starts automatically


" """ configuration for quick-scope ------------------------
" """ color optimal letters to move using the jump-keys (tfTF;,) (only vimscript)
" source: 'https://github.com/unblevable/quick-scope'
" plugin starts automatically
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']


" """ configuration for Cheat40 ----------------------------
" """ display infos on how to use vim (only viscript)
" source: 'https://github.com/lifepillar/vim-cheat40'
" plugin has to be started manually


" """ configuration for csapprox ---------------------------
" """ add support for color schemes independent of the used terminal (only vimscript)
" source: 'https://github.com/godlygeek/csapprox'
" plugin starts automatically


" """ configuration for colorizer --------------------------
" """ display colors directly in-line (only vimscript)
" source: 'https://github.com/lilydjwg/colorizer'
" plugin starts automatically
let g:colorizer_maxlines = 1000



" """ configuration for colorschemes -----------------------
" """ start by loading the default colorscheme (to reset all custom options)
" for more information see also 'https://gist.github.com/XVilka/8346728'
" """ colorthemes (only vimscript and shell)
" source: 'https://gitlab.com/protesilaos/tempus-themes-vim'
" source: 'https://github.com/romainl/vim-dichromatic'
" source: 'https://github.com/romainl/Apprentice'
" source: 'https://github.com/markeganfuller/vim-journeyman'
" source: 'https://github.com/tpope/vim-vividchalk'
" source: 'https://github.com/heraldofsolace/nisha-vim'
" source: 'https://github.com/tomasr/molokai'
" source: 'https://github.com/hardselius/warlock'
let g:molokai_original = 0
let g:rehash256 = 1


try
  colorscheme slate
  silent! colorscheme dichromatic
  silent! colorscheme vividchalk
  silent! colorscheme nisha
  silent! colorscheme warlock
  silent! colorscheme apprentice
  silent! colorscheme journeyman
  silent! colorscheme tempus_classic
  silent! colorscheme molokai
catch
  colorscheme default
  silent! colorscheme tempus_totus
endtry

