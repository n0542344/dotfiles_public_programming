" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  x0ff manually created  """ "
" """        dotfiles         """ "
" """       2020-08-10        """ "

" """ """ .config/nvim/plugins.vim (non-standard, no dependencies) =============



" have a custom file to source for plugins to be sourced independently



" Load packager only when you need it
function! PackagerInit() abort
  packadd vim-packager
  call packager#init()
  call packager#add('kristijanhusak/vim-packager', { 'type': 'opt' })


" """ base system interaction --------------------------------------------------

" enhanced usability of netrw
  call packager#add('https://github.com/tpope/vim-vinegar')

" fuzzy search for files within a directory from within vim
  call packager#add('https://github.com/ctrlpvim/ctrlp.vim')

" UNIX shell command interaction from within vim
  call packager#add('https://github.com/tpope/vim-eunuch')

" OSC52 clipboard synchronization
  call packager#add('https://github.com/greymd/oscyank.vim')

" change root directory to vcs-directory
  call packager#add('https://github.com/airblade/vim-rooter')

" interact with system grepping tool
  call packager#add('https://github.com/mhinz/vim-grepper')


" """ eyecandy -----------------------------------------------------------------

" colourful vim status line
  call packager#add('https://github.com/itchyny/lightline.vim')

" highlight yanked areals
  call packager#add('https://github.com/machakann/vim-highlightedyank')

" highlight marks
  call packager#add('https://github.com/kshenoy/vim-signature')

" color parentheses using different colors per level
  call packager#add('https://github.com/frazrepo/vim-rainbow')

" color trailing whitespace
  call packager#add('https://github.com/inkarkat/vim-ShowTrailingWhitespace')

" color optimal letters to move using the jump-keys (tfTF;,)
  call packager#add('https://github.com/unblevable/quick-scope')

" add a vim cheatsheet
  call packager#add('https://github.com/lifepillar/vim-cheat40')

" add support for color schemes independent of the used terminal
  call packager#add('https://github.com/godlygeek/csapprox')

" display colors directly in-line
  call packager#add('https://github.com/lilydjwg/colorizer')


" add colorthemes for vim
  call packager#add('https://gitlab.com/protesilaos/tempus-themes-vim')
  call packager#add('https://github.com/tpope/vim-vividchalk')
  call packager#add('https://github.com/noahfrederick/vim-noctu')
  call packager#add('https://github.com/jeffkreeftmeijer/vim-dim')
  call packager#add('https://github.com/romainl/Apprentice')
  call packager#add('https://github.com/markeganfuller/vim-journeyman')
  call packager#add('https://github.com/tomasr/molokai')
  call packager#add('https://github.com/heraldofsolace/nisha-vim')
  call packager#add('https://github.com/hardselius/warlock')
  call packager#add('https://github.com/romainl/vim-dichromatic')



" """ viewing ------------------------------------------------------------------

" create a narrow region of text
  call packager#add('https://github.com/chrisbra/NrrwRgn')

" make table-like indentations
  call packager#add('https://github.com/godlygeek/tabular')
  call packager#add('https://github.com/tommcdo/vim-lion')

" working with csv-files
  call packager#add('https://github.com/chrisbra/csv.vim')

" enable distraction-free writing in vim
  call packager#add('https://github.com/junegunn/goyo.vim')

" enable distraction-free surrounding in vim
  call packager#add('https://github.com/junegunn/limelight.vim')

" create a temporary scratch buffer
  call packager#add('https://github.com/mtth/scratch.vim')


" """ editing ------------------------------------------------------------------

" add readline keybindings for vim
  call packager#add('https://github.com/tpope/vim-rsi')

" create an undo-tree-structure (not saved on disk)
  call packager#add('https://github.com/mbbill/undotree')

" automatic creation of paired symbols (brackts, quotes, ...)
  call packager#add('https://github.com/jiangmiao/auto-pairs')

" add mnemonics for often used commands (jumping, adding lines, ...)
  call packager#add('https://github.com/tpope/vim-unimpaired')

" add a more powerful search mode
  call packager#add('https://github.com/easymotion/vim-easymotion')

" use '*' and '#' when visually selecting
  call packager#add('https://github.com/bronson/vim-visual-star-search')

" swap parts of a line
  call packager#add('https://github.com/machakann/vim-swap')

" change the surroundings (brackets, quotes, ...)
  call packager#add('https://github.com/tpope/vim-surround')

" repeat actions on more complex operations
  call packager#add('https://github.com/tpope/vim-repeat')

" define additional text objects
  call packager#add('https://github.com/wellle/targets.vim')

" automatic creation of comments depending on file type
  call packager#add('https://github.com/tpope/vim-commentary')

" coerce variable names
  call packager#add('https://github.com/tpope/vim-abolish')

" improved completion features for vim
  call packager#add('https://github.com/ajh17/VimCompletesMe')

" add option to work with dates in vim
  call packager#add('https://github.com/tpope/vim-speeddating')

" add option to kill buffers without changing the split
  call packager#add('https://github.com/qpkorr/vim-bufkill')

" CAVE: 'quickfix-reflector.vim' and 'quickr-preview.vim' are conflicting!
" perform changes in the quickfix buffer
  "call packager#add('https://github.com/stefandtw/quickfix-reflector.vim')

" enable quickfix buffer preview
  call packager#add('https://github.com/ronakg/quickr-preview.vim')


" """ programming ------------------------------------------

" interaction between vim, python/shell/other REPL languages and tmux
  call packager#add('https://github.com/jpalardy/vim-slime')
  call packager#add('https://gitlab.com/HiPhish/repl.nvim')
  call packager#add('https://github.com/kassio/neoterm')
" call packager#add('https://github.com/jalvesaq/vimcmdline')

" automatic REPL for files
  call packager#add('https://github.com/metakirby5/codi.vim')


" """ snippets integration ---------------------------------

" write LaTeX-commands quickly
  call packager#add('https://github.com/brennier/quicktex')

" snippets manager for vim
  call packager#add('https://github.com/garbas/vim-snipmate')

" utility functions for vim (dependency of vim-snipmate)
  call packager#add('https://github.com/tomtom/tlib_vim')

" interpret a file by function and cache file automatically (dependency of vim-snipmate)
  call packager#add('https://github.com/marcweber/vim-addon-mw-utils')

" snippets for vim-snipmate (and UltiSnips)
  call packager#add('https://github.com/honza/vim-snippets')


" " UltiSnips completion framework (vimscript and python)
"   call packager#add('https://github.com/sirver/UltiSnips')


" " """ VSCode snippets (using LSP, currently disabeled, since not working)
"
" " snippets plugin
" "  call packager#add('https://github.com/hrsh7th/vim-vsnip')
"
" " integration for snippets plugin to lsp
" "  call packager#add('https://github.com/hrsh7th/vim-vsnip-integ')



" """ language integration ---------------------------------

" enable access to the language server protocol
"  call packager#add('https://github.com/prabirshrestha/vim-lsp/')
  call packager#add('prabirshrestha/vim-lsp')

" configuration for python using vim-lsp
  call packager#add('https://github.com/ryanolsonx/vim-lsp-python')

" enable an easy installation and configuration for language server protocols
  call packager#add('https://github.com/mattn/vim-lsp-settings')

" normalize async job-controls for vim
  call packager#add('https://github.com/prabirshrestha/async.vim')

" async completion framework
  call packager#add('https://github.com/prabirshrestha/asyncomplete.vim')

" language server protocol source for async completion framework
  call packager#add('https://github.com/prabirshrestha/asyncomplete-lsp.vim')

" source file for asyncomplete - tags
  call packager#add('https://github.com/prabirshrestha/asyncomplete-tags.vim')

" source file for asyncomplete - buffer
  call packager#add('https://github.com/prabirshrestha/asyncomplete-buffer.vim')

" source file for asyncomplete - file
  call packager#add('https://github.com/prabirshrestha/asyncomplete-file.vim')

" source file for asyncomplete - omnicomplete
  call packager#add('https://github.com/yami-beta/asyncomplete-omni.vim')


" " connect completion support between vim-lsp and ncm2 (duplicate)
"   call packager#add('https://github.com/ncm2/ncm2-vim-lsp')



" " """ Language Server Protocol (LSP) support for vim and neovim
"   call packager#add('https://github.com/autozimu/LanguageClient-neovim')
" " CAVE: has to be installed manually




endfunction


command!        PackagerInstall call    PackagerInit() | call packager#install()
command! -bang  PackagerUpdate  call    PackagerInit() | call packager#update({ 'force_hooks': '<bang>' })
command!        PackagerClean   call    PackagerInit() | call packager#clean()
command!        PackagerStatus  call    PackagerInit() | call packager#status()






" """ """ """ ------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ """ ------------------------------------------------------------------



" """ """ ----------------------------------------------------------------------
" """ base system interaction --------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_base-system-interaction.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_base-system-interaction.vim
endif


" """ configuration for vinegar ----------------------------
" """ configuration for ctrlp.vim --------------------------
" """ configuration for vim-eunuch -------------------------
" """ configuration for oscyank ----------------------------
" """ configuration for vim-rooter -------------------------



" """ """ ----------------------------------------------------------------------
" """ eyecandy -----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_eyecandy.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_eyecandy.vim
endif


" """ configuration for lightline.vim ----------------------
" """ configuration for vim-highlightedyank ----------------
" """ configuration for vim-signature ----------------------
" """ configuration for rainbow ----------------------------
" """ configuration for ShowTrailingWhitespace -------------
" """ configuration for quick-scope ------------------------
" """ configuration for Cheat40 ----------------------------
" """ configuration for csapprox ---------------------------
" """ configuration for colorizer --------------------------
" """ configuration for colorschemes -----------------------



" """ """ ----------------------------------------------------------------------
" """ viewing ------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_viewing.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_viewing.vim
endif


" """ configuration for NrrwRgn ----------------------------
" """ configuration for tabular ----------------------------
" """ configuration for vim-lion ---------------------------
" """ configuration for goyo.vim ---------------------------
" """ configuration for limelight.vim ----------------------
" """ configuration for scratch ----------------------------



" """ """ ----------------------------------------------------------------------
" """ editing ------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_editing.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_editing.vim
endif


" """ configuration for vim-rsi ----------------------------
" """ configuration for undotree ---------------------------
" """ configuration for vim-surround -----------------------
" """ configuration for vim-repeat -------------------------
" """ configuration for targets.vim ------------------------
" """ configuration for auto-pairs -------------------------
" """ configuration for vim-unimpaired ---------------------
" """ configuration for vim-easymotion ---------------------
" """ configuration for vim-visual-star-search -------------
" """ configuration for vim-swap ---------------------------
" """ configuration for vim-commentary ---------------------
" """ configuration for vim-abolish ------------------------
" """ configuration for VimCompletesMe ---------------------
" """ configuration for vim-speeddating --------------------
" """ configuration for quickfix-reflector -----------------
" """ configuration for quickr-preview ---------------------
" """ configuration for vim-bufkill ------------------------




" """ """ ----------------------------------------------------------------------
" """ programming --------------------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_programming.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_programming.vim
endif


" """ configuration of vim-slime ---------------------------
" """ configuration of repl.nvim ---------------------------
" """ configuration of neoterm -----------------------------
" " """ configuration of vimcmdline --------------------------
" """ configuration of codi --------------------------------




" """  """ ----------------------------------------------------------------------
" """ snippets integration -----------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_snippets.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_snippets.vim
endif


" """ configuration of quicktex ----------------------------
" """ configuration of vim-snipmate ------------------------
" """ configuration of tlib_vim ----------------------------
" """ configuration of vim-addon-mw-utils ------------------
" """ configuration of vim-snippets ------------------------
" " """ configuration of UltiSnips ---------------------------







" """ """ ----------------------------------------------------------------------
" """ completion framework -----------------------------------------------------
" """ """ ----------------------------------------------------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_completion.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_completion.vim
endif



" """ configuration of vim-lsp -----------------------------
" """ configuration of vim-lsp-settings --------------------
" """ configuration of vim-lsp-python ----------------------
" """ configuration of async.vim ---------------------------
" """ configuration of asyncomplete.vim --------------------
" """ configuration of asyncomplete-tags.vim ---------------
" """ configuration of asyncomplete-buffer.vim -------------
" """ configuration of asyncomplete-file.vim ---------------
" """ configuration of asyncomplete-omni.vim ---------------
" " """ configuration of ncm2-vim-lsp ------------------------
" " """ configuration of LanguageClient-neovim ---------------



