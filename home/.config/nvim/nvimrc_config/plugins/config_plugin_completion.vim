" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ completion framework -----------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration of vim-lsp -----------------------------
" """ enable access to the language server protocol (only vimscript)
" source: 'https://github.com/prabirshrestha/vim-lsp/' (needs to be loaded as 'prabirshrestha/vim-lsp' for some unclear reason)
" plugin starts automatically (depending on the filetype)
let g:lsp_auto_enable = 1                   " enable plugin
let g:lsp_diagnostics_enabled = 1           " enable diagnostics support
let g:lsp_preview_autoclose = 1				" close preview when moving the cursor (it not working, use 'Ctrl-w z' or ':pc' to close manually)
let g:lsp_signs_enabled = 1                 " enable signs
let g:lsp_signs_priority = 11               " define signs priority (10 is the default)
let g:lsp_diagnostics_echo_cursor = 1       " enable echo under cursor when in normal mode
let g:lsp_diagnostics_echo_delay = 1000		" increase timeout until echo is displayed
let g:lsp_highlights_enabled = 1            " enable highlighting of diagnostics
let g:lsp_highlight_references_enabled = 1  " enable highlighting of the references to the symbol under the cursor
let g:lsp_virtual_text_enabled = 0          " disable virtual text next to the warning
set signcolumn=auto
autocmd CompleteDone * silent! pclose       " automatically close preview window if completion is finished
" CAVE: details of the languageserver-configuration depend on the used lanuageserver!
" for R: should be used together with the R-package 'languageserver' (easier when using 'vim-lsp-settings' plugin)
" alternatively try the R-package 'languageserversetup' and follow the instructions
" CAVE: the R-package 'languageserver' needs a network port! Might not work if a firewall is used!

" " alternative to using 'vim-lsp-python'
" if executable('pyls')
"       au User lsp_setup call lsp#register_server({
"               \ 'name': 'pyls',
"               \ 'cmd': {server_info->['pyls']},
"               \ 'whitelist': ['python'],
"               \ })
"     endif

" """ configuration of vim-lsp-settings --------------------
" """ enable an easy installation and configuration for language server protocols (only vimscript (and sh/make for install))
" source: 'https://github.com/mattn/vim-lsp-settings'
" plugin has to be used manually
" {{{ vim-lsp-settings usage: 
" :LspInstallServer		" install server for the language currently using
" }}}


" """ configuration of vim-lsp-python ----------------------
" """ Sets up vim-lsp for Python (only vimscript)
" needs python-language-server (pip3 install python-language-server)
" source: 'https://github.com/ryanolsonx/vim-lsp-python'


" """ configuration of async.vim ---------------------------
" """ normalize async job-controls for vim (only vimscript (and sh for test))
" source: 'https://github.com/prabirshrestha/async.vim'
" plugin starts automatically


" """ configuration of asyncomplete.vim --------------------
" """ async completion framework (only vimscript)
" source: 'https://github.com/prabirshrestha/asyncomplete.vim'
" CAVE: this package does not contain any sources - these have to be added manually


" """ configuration of asyncomplete-lsp.vim ----------------
" """ language server protocol source for async completion framework
" source: 'https://github.com/prabirshrestha/asyncomplete-lsp.vim'
" plugin starts automatically


" """ configuration of asyncomplete-tags.vim ---------------
" """ source file for asyncomplete - tags (only vimscript)
" source: 'https://github.com/prabirshrestha/asyncomplete-tags.vim'
" plugin starts automatically
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#tags#get_source_options({
    \ 'name': 'tags',
    \ 'whitelist': ['c'],
    \ 'completor': function('asyncomplete#sources#tags#completor'),
    \ 'config': {
    \    'max_file_size': 50000000,
    \  },
    \ }))


" """ configuration of asyncomplete-buffer.vim -------------
" """ source file for asyncomplete - buffer (only vimscript)
" source: 'https://github.com/prabirshrestha/asyncomplete-buffer.vim'
" plugin starts automatically
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
    \ 'name': 'buffer',
    \ 'whitelist': ['*'],
    \ 'blacklist': ['go'],
    \ 'completor': function('asyncomplete#sources#buffer#completor'),
    \ 'config': {
    \    'max_buffer_size': 5000000,
    \  },
    \ }))


" """ configuration of asyncomplete-file.vim ---------------
" """ source file for asyncomplete - file (only vimscript)
" source: 'https://github.com/prabirshrestha/asyncomplete-file.vim'
" plugin starts automatically
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'whitelist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))


" """ configuration of asyncomplete-omni.vim ---------------
" """ source file for asyncomplete - omnicomplete (only vimscript)
" source: 'https://github.com/yami-beta/asyncomplete-omni.vim'
" plugin starts automatically
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
    \ 'name': 'omni',
    \ 'whitelist': ['*'],
    \ 'blacklist': ['c', 'cpp', 'html'],
    \ 'completor': function('asyncomplete#sources#omni#completor'),
    \ 'config': {
    \   'show_source_kind': 1
    \ }
    \ }))

" Add `o` kind label to `'menu'`

" au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
"     \ 'name': 'omni',
"     \ 'whitelist': ['*'],
"     \ 'blacklist': ['c', 'cpp', 'html'],
"     \ 'completor': function('asyncomplete#sources#omni#completor')
"     \  }))




" " """ configuration of ncm2-vim-lsp ------------------------
" " """ connect completion support between vim-lsp and ncm2 (written in only vimscript, but needs external dependency - ncm and vim-lsp)
" " source: 'https://github.com/ncm2/ncm2-vim-lsp'





" " """ configuration of LanguageClient-neovim ---------------
" " """ Language Server Protocol (LSP) support for vim and neovim (vimscript, rust, python shell)
" " source: 'https://github.com/autozimu/LanguageClient-neovim'
" " additional information: 'https://github.com/autozimu/LanguageClient-neovim/blob/next/INSTALL.md'
" " overview over the available servers at 'https://langserver.org/'
" " CAVE: manual installation of language servers needed! Instructions can be found above
" " {{{ LanguageClient-neovim usage:
" " dependencies for R can be installed using "Rscript -e 'install.packages("languageserver")''"
" " more information on https://github.com/REditorSupport/languageserver
" " dependencies for Python can be installed using "pip3 install 'python-language-server[all]'"
" " more information on https://github.com/palantir/python-language-server
" " dependencies for bash can be installed using "npm i -g bash-language-server"
" " CAVE: dependencies for bash need npm (which is HUGE, therefore not installed)
" " more information on https://github.com/bash-lsp/bash-language-server
" " if installed, the code could be added like that:
" "   \ 'sh':             [ 'bash-language-server', 'start' ],
" " }}}
" " necessary prequisites (not plugin-specific)
" set runtimepath+=~/.config/nvim/pack/minpac/start/LanguageClient-neovim
" set hidden
"
" let g:LanguageClient_serverCommands = {
"     \ 'r':              [ 'R', '--slave', '-e', 'languageserver::run()' ],
"     \ 'python':         [ 'pyls' ],
"     \ }
"
" " show linting-information if possible
" set signcolumn=auto
"
" " don't use virtual text next to linting information (will be shown in the status line below)
" let g:LanguageClient_useVirtualText = "CodeLens"
"
" " nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" " " Or map each action separately
" " nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" " nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
" " nnoremap <silent> gf :call LanguageClient_textDocument_formatting()<CR>
" " nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>


