" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ formatter ----------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for neoformat --------------------------
" """ format code (vimscript/python/shell, but needs external dependency - formatters (e.g. shftm))
" source: 'https://github.com/sbdchd/neoformat'
" plugin has to be used manually
" {{{ neoformat usage:
" Supported Filetypes:
"   LaTeX:              latexindent
"   Markdown:           remark, prettier
"   Pandoc Markdown:    pandoc
"   Python:             yapf, autopep8, black, pydevf, isort, docformatter, pyment
"   R:                  styler, formatR
"   Shell:              shfmt
"   YAML:               pyaml, prettier
" :Neoformat            format file using the default filetype
" :Neoformat styler     format file using the formatter 'styler'
" :Neoformat! r         format file or visual selection using the default for ft=r
" }}}
" use &formatprg as a formatter
let g:neoformat_try_formatprg = 1
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to spaces conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1


" """ configuration for vim-shfmt --------------------------
" """ sh-formatter for neoformat (written in only vimscript, but needs external dependency - shfmt -> not available in Debian standard repositories)
" additional dependency: 'https://github.com/mvdan/sh'
" source: 'https://github.com/z0mbix/vim-shfmt'
" plugin has to be used manually
" use speces instead of tabs
"let g:shfmt_extra_args = '-i 2'


