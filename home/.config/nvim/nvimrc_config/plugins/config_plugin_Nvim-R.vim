" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ Nvim-R -------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------



" """ configuration for Nvim-R -----------------------------
" """ interaction between vim, R and tmux (not only vimscript, also R, python and c)
" source: 'https://github.com/jalvesaq/Nvim-R'
" more information 'https://raw.githubusercontent.com/jalvesaq/Nvim-R/master/doc/Nvim-R.txt'
" plugin has to be used manually
" {{{ Nvim-R usage:
"   \rf             Start R (default)
"   \rq             Close R (no save)
"   \ss             Selection
"   \l              Line
"   \d              Line (and down)
"   \r<Right>       Right part of line (cur)
"   \su             All lines above the current one
"   __              Insert "<-"
"   CTRL-X CTRL-O   Complete object name (insert mode)
"   CTRL-X CTRL-A   Complete argument name (insert mode)
"   \rv             View data.frame (cur) in new tab
"   \vh             View head(data.frame) (cur) in horizontal split
"   \o              Line (evaluate and insert the output as comment)
"   \rl             List objects in environment
"   \xx             Toggle comment (line, sel)
"   \rh             Help (cur)
"   \rs             Summary (cur)
" for more information see separate file
" }}}
" press __ to have Nvim-R insert the assignment operator: <-
let R_assign_map = "__"
" map <Esc> to go into normal mode in the terminal instad of CTRL-\ CTRL-n
let R_esc_term = 1
" have the R_console run in a buffer, which is included in the bufferlist, but with fixed width
let R_buffer_opts = "winfixwidth buflisted"
" set a minimum source editor width
let R_min_editor_width = 85
" display R-objects
" let R_objbr_place = 'console,right'
let R_objbr_place = 'console,below'
let R_objbr_w = 85
let R_objbr_h = 999
let R_objbr_opendf = 1    " Show data.frames elements
let R_objbr_openlist = 1  " Show lists elements
let R_objbr_allnames = 1  " Show .GlobalEnv hidden objects
let R_objbr_labelerr = 1  " Warn if label is not a valid text
" display R-help
let R_nvimpager = 'tab'
" let R_nvimpager = 'vertical'
let R_editor_w = 85
let R_editor_h = 999
let R_help_w = 85
" autocmd VimResized * let R_rconsole_width = winwidth(0) / 2 " automatically resize window to 50% of screen size
let R_rconsole_width = 85
" let R_rconsole_width = 0   " perform horizontal split
let R_rconsole_height = 20
" define prompt string and continuation string
let g:Rout_prompt_str = '» '
let g:Rout_continue_str = '± '
" don't color the terminal output
let Rout_more_colors = 0
let rout_follow_colorscheme = 1
" highlight function names only if they are followed by a parenthesis
let R_hi_fun_paren = 1
" make sure the console is at the bottom by making it really wide
"let R_rconsole_width = 1000
" show arguments for functions during omnicompletion (no longer available)
"let R_show_args = 1
" Don't expand a dataframe to show columns by default
let R_objbr_opendf = 0

" Press the space bar to send lines and selection to R console
vmap <Space> <Plug>RDSendSelection
nmap <Space> <Plug>RDSendLine

" create ctags-file (need r-package 'utils' and 'nvimcom')
" command not really needed if 'universal-ctags' is installed (which can also be combined with vim-gutentags)
:command! Rtags :!Rscript -e 'rtags(path="./", recursive=TRUE, ofile="RTAGS")' -e 'etags2ctags("RTAGS", "rtags")' -e 'unlink("RTAGS")'
set tags+=rtags



" """ configuration ov R-Vim-runtime -----------------------
" """ R-related runtimes for vim (not only vimscript, also R)
" source: 'https://github.com/jalvesaq/R-Vim-runtime'
" {{{ usage R-Vim-runtime:
" :help ft-r-indent                     " see information on the indentation
" }}}



" """ configuration of vim-rmarkdown -----------------------
" """ interacting between R, pandoc, Rmarkdown and vim (not only vimscript, also R)
" needs plugins 'vim-pandoc and 'vim-pandoc-syntax' as well as the 'Rmarkdown'-package in R
" source: 'https://github.com/vim-pandoc/vim-rmarkdown'
" {{{ vim-rmarkdown usage:
" :RMarkdown pdf                        " generates a pdf-document by executing Rscript -e 'library(rmarkdown);render('input.Rmd', 'pdf_document')'
" :RNrrw                                " if vim-package 'NrrwRgn' is installed, this command creates a new buffer from the current chunk
" }}}
" CAVE: this plugin interferes with the chunk-detection of Nvim-R!





" """ sources ------------------------------------------------------------------
" <https://hpcc.ucr.edu/manuals_linux-cluster_terminalIDE.html>
" <https://raw.githubusercontent.com/jalvesaq/Nvim-R/master/doc/Nvim-R.txt>
" <https://github.com/jalvesaq/Nvim-R>
" <https://sherif.io/2017/07/22/nvim-r.html>
" <https://www.freecodecamp.org/news/turning-vim-into-an-r-ide-cd9602e8c217/>
" <https://github.com/gaalcaras/ncm-R>

