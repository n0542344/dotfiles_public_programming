" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ tags ---------------------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration of vim-gutentags -----------------------
" """ continuous generation of tags (only vimscript (and universal-ctags with sh))
" source: 'https://github.com/ludovicchabant/vim-gutentags'
" plugin starts automatically
" might need universal-ctags installed (see also https://ctags.io/ and https://bolt80.com/gutentags/)
set statusline+=%{gutentags#statusline()}

" depending on the distribution it might be necessary to specify
" the name of the used binary for the universal-ctags
if executable('ctags')
  let g:gutentags_ctags_executable = 'ctags'
endif

if executable('universal-ctags')
  let g:gutentags_ctags_executable = 'universal-ctags'
endif

