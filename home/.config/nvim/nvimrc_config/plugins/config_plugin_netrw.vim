" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ non-installed plugins ----------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration of netrw (part ob base vim) ------------
" """ file navigation within standard vim
" part of the base system (no external dependency)
" """ {{{ netrw usage:
" :Explore                              " opens netrw in the current window
" :Sexplore                             " opens netrw in a horizontal split
" :Vexplore                             " opens netrw in a vertical split
" :Texplore                             " opens netrw in a new tab
" :edit .                               " or any other folder name
" v                                     " open file in vertical split
" t                                     " open file in new tab
" I                                     " toggle header
" gh                                    " toggle hidden files
" }}}
let g:netrw_banner = 0                  " disable banner for netrw
let g:netrw_brose_split = 4             " open in prior window
let g:netrw_altv = 1                    " open splits to the right
let g:netrw_liststyle = 3               " use tree view vor netrw, change by typing i
let g:netrw_winsize = 25                " fix size of window to 25%
let g:netrw_list_hide=netrw_gitignore#Hide()    " ignore files in gitignore
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'  " igrnoe files using the following syntax
let g:netrw_fastbrowse = 0              " enable quitting out of netrw when it is the last window (alternatively use :qa!)

" " display files similar to NERDtree by using netrw
" augroup ProjectDrawer
"     autocmd!
"     autocmd VimEnter * :Vexplore
" augroup END




" """ sources ------------------------------------------------------------------
" <https://shapeshed.com/vim-netrw/>
" <https://blog.stevenocchipinti.com/2016/12/28/using-netrw-instead-of-nerdtree-for-vim/>
