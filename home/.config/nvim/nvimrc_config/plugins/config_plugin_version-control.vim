" """ """ ----------------------------------------------------------------------
" """ """ plugin configurations ------------------------------------------------
" """ """ ----------------------------------------------------------------------

" """ """ ----------------------------------------------------------------------
" """ version control -----------------------------------------------------------
" """ """ ----------------------------------------------------------------------


" """ configuration for vim-fugitive -----------------------
" """ interact with git (written in only vimscript, but needs external dependency - git) :TODO: TO_MOVE
" source: 'https://github.com/tpope/vim-fugitive'
" more information: 'http://vimcasts.org/episodes/fugitive-vim-browsing-the-git-object-database/'
" plugin has to be used manually
" {{{ vim-fugitive usage:
" :Git [argument]                                   - use any git command available from the command line
" :Gstatus (git status), then g?                    - bring up a list of maps for numerous operations including diffing, staging, committing, rebasing, and stashing.
" :Gedit (and :Gsplit, :Gvsplit, :Gtabedit, ...)    - view any blob, tree, commit, or tag in the repository. Edit a file in the index and write to it to stage the changes. 
" :Gdiffsplit                                       - bring up the staged version of the file side by side with the working tree version stage a subset of the file's changes.
" :Gcommit, :Gmerge, and :Grebase                   - commit, merge, and rebase 
" :Gpush, :Gfetch, and :Gpull                       - send and retrieve upstream changes.
" :Gblame                                           - brings up an interactive vertical split with git blame output.
" :Gmove FILE                                       - does a git mv on a file and simultaneously renames the buffer (FILE relative from location of file, /FILE relative to git repository)
" :Gdelete                                          - does a git rm on a file and simultaneously deletes the buffer.
" :Ggrep                                            - search the work tree (or any arbitrary commit) with git grep, skipping over that which is not tracked in the repository. 
" :Gclog and :Gllog                                 - load all previous commits into the quickfix or location list.
" :Gread (variant of git checkout)                  - operates on the buffer rather than the filename. Can be used to undo operations, but CAVE: no warnings!
" :Gwrite                                           - writes to both the work tree and index versions of a file, (like git add and like git checkout combined)
" }}}
" automatically delete buffers from fugitive, if they are hidden
autocmd BufReadPost fugitive://* set bufhidden=delete
" display fugitive-information in the statusline
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P


" """ configuration for gv ---------------------------------
" """ add 'tig'-like browser to fugitive (written in only vimscript, but needs external dependency - git) :TODO: TO_MOVE
" source: 'https://github.com/junegunn/gv.vim'
" {{{ gv usage:
" :GV                   	- to open commit browser. You can pass git log options to the command, e.g. :GV -S foobar.
" :GV!                  	- will only list commits that affected the current file
" :GV?                  	- fills the location list with the revisions of the current file
" :GV or :GV?           	- can be used in visual mode to track the changes in the selected lines.
" o or <cr> on a commit 	- display the content of it
" o or <cr> on commits  	- display the diff in the range
" O                     	- opens a new tab instead
" gb                    	- for :Gbrowse
" ]] and [[             	- move between commits
" .                     	- to start command-line with :Git [CURSOR] SHA à la fugitive
" q or gq               	- close
" }}}


" """ configuration for vim-signify ------------------------
" """ git-integration using chunk-wise display of changes (written in only vimscript (tests are in bash), but needs external dependency - vcs-software (e.g. git)) :TODO: TO_MOVE
" source: 'https://github.com/mhinz/vim-signify'
" display of changes within document, for multiple vcs, no option to stage
" {{{ vim-signify usage:
" ]c                        - Jump to the next hunk.
" [c                        - Jump to the previous hunk.
" }}}
let g:signify_disable_by_default = 0
let g:signify_sign_show_count = 0
set updatetime=200


" """ configuration for vim-gitgutter ----------------------
" """ view git information (written in only vimscript (tests are in bash), but needs external dependency - git) :TODO: TO_MOVE
" source: 'https://github.com/airblade/vim-gitgutter'
" plugin has to be started manually
" {{{ gitgutter usage:
" ]c                        - jump to next hunk (change)        - CAVE: changed defaults, since it caused problems (identical usability)
" [c                        - jump to previous hunk (change)    - CAVE: changed defaults, since it caused problems (identical usability)
" <Leader>hs                - stage an individual hunk when your cursor is in it
" :GitGutterStageHunk       - stage the predefinded hunk
" <Leader>hu                - undo it
" :42,45GitGutterStageHunk  - stage an additions-only hunk
" <Leader>hp                - preview the hunk
" :wincmd P                 - move to the preview window
" }}}
" automatically start gitgutter
let g:gitgutter_enabled = 0
"let g:gitgutter_highlight_linenrs = 1
" changed default, since it caused problems
"let g:gitgutter_map_keys = 0
"nmap [c <Plug>(GitGutterPrevHunk)
"nmap ]c <Plug>(GitGutterNextHunk)
"nnoremap [c :GitGutterPrevHunk<cr>
"nnoremap ]c :GitGutterNextHunk<cr>


