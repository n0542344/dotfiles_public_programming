" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  x0ff manually created  """ "
" """        dotfiles         """ "
" """       2020-10-11        """ "

" """ """ .config/nvim/plugins.vim =============================================



" have a custom file to source for plugins to be sourced independently


" """ Installation when using neovim (or vim, see 2nd line) --------------------
"
"git clone https://github.com/kristijanhusak/vim-packager ~/.config/nvim/pack/packager/opt/vim-packager
"git clone https://github.com/kristijanhusak/vim-packager ~/.vim/pack/packager/opt/vim-packager
"
" Packager utilizes jobs feature to the maximum, and runs everything that it
" can in a job, and shows whole process in the separate window.  It is written
" completely in vimscript.
"

" """ use plugins in neovim with vim-packager ----------------------------------
"
if !isdirectory($HOME . "/.config/nvim/pack")
    call  mkdir($HOME . "/.config/nvim/pack", "p", 0700)
endif
"
"
" """ use plugins directory vom regular vim ------------------------------------
"
"if !isdirectory($HOME . "/.vim/pack")
"    call  mkdir($HOME . "/.vim/pack", "p", 0700)
"endif
"


" """ configuration for netrw (included in base vim) ---------------------------

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/config_plugin_netrw.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/config_plugin_netrw.vim
endif


" """ read configuration file concerning plugins with no external dependencies -

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/plugins_no_deps.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/plugins_no_deps.vim
endif



" """ read configuration file concerning plugins with trivial dependencies -----

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/plugins_deps_trivial.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/plugins_deps_trivial.vim
endif



" """ read configuration file concerning plugins with complex dependencies -----

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins/plugins_deps_complex.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins/plugins_deps_complex.vim
endif

