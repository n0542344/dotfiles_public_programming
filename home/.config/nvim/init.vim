" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  ! manually  created !  """ "
" """        dotfiles         """ "
" """       2020-07-06        """ "

" """ """ .config/nvim/init.vim ================================================


"set lines=35 columns=100                " define size of vim if opened


" """ define path and sourcefiles for nvim """ ---------------------------------

set runtimepath +=~/.config/nvim         " default location

if !isdirectory($HOME . "/.config/nvim/pack")
    call  mkdir($HOME . "/.config/nvim/pack", "p", 0700)
endif
set packpath    +=~/.config/nvim         " default plugin location for vim (automatically searches the 'pack/'-subdirectory for plugins)

"source ~/.vimrc


" """ generate backups, temporary files and undos ------------------------------

if !isdirectory($HOME . "/.vim/tmp/swap")
    call  mkdir($HOME . "/.vim/tmp/swap", "p", 0700)
endif
set directory=~/.vim/tmp/swap//


if !isdirectory($HOME . "/.vim/tmp/backup")
    call  mkdir($HOME . "/.vim/tmp/backup", "p", 0700)
endif
set backup
set backupdir=~/.vim/tmp/backup//


if !isdirectory($HOME . "/.vim/tmp/undo")
    call  mkdir($HOME . "/.vim/tmp/undo", "p", 0700)
endif
set undofile
set undodir=~/.vim/tmp/undo//
set undolevels=1000


if !isdirectory($HOME . "/.vim/tmp/viminfo")
    call  mkdir($HOME . "/.vim/tmp/viminfo", "p", 0700)
endif
set viminfo=%,<1000,'10,/20,:100,h,f0,n~/.vim/tmp/viminfo/.viminfo.shada


" """ source the file nvimrc_config/plugins.vim --------------------------------

if !isdirectory($HOME . "/.config/nvim/nvimrc_config")
    call  mkdir($HOME . "/.config/nvim/nvimrc_config", "p", 0700)
endif

if filereadable($HOME . "/.config/nvim/nvimrc_config/plugins.vim")
    source      $HOME/.config/nvim/nvimrc_config/plugins.vim
endif


" """ general options ----------------------------------------------------------

let mapleader = "\\"                    " use '\' as leader, the second '\' is to escape
let maplocalleader = "\\"               " use '\' as local leader, the second '\' is to escape

set timeout timeoutlen=2000             " how long until a key sequence is pressed?
set ttimeout ttimeoutlen=200            " how long after a TUI input is pressed?

set fileformats=unix                    " support all three newline formats
set encoding=utf-8                      " define standard file encoding

syntax on                               " show syntax
set synmaxcol=300                       " perform syntax analysis only on 300 lines

set visualbell                          " visual signal in case of errors

set lazyredraw                          " redraw only if necessary (increases speed)
"set ttyfast                             " set a fast terminal (not needed in nvim, since always on)


" """ help orientation within file ---------------------------------------------

set number                              " add line number information and make it relative to the cursor position
                                        " could be abbreviated using set nu
set relativenumber                      " displays current line number, the rest ist relative to that line
                                        " CAVE: this option is resource-intensive, disable for higher speed

set ruler                               " add information on cursor position (default on)
set cursorline                          " add line mark on current cursor position
                                        " CAVE: this option is resource-intensive, disable for higher speed
set cursorcolumn                        " add column mark on current cursor position
                                        " CAVE: this option is resource-intensive, disable for higher speed

" scoll -commands can only be used if wrapping is disabled
set scrolloff=5                         " keep at least 5 lines above/below
set sidescrolloff=5                     " keep at least 5 lines left/right

set splitbelow                          " start splitscreen below
set splitright                          " start splitscreen to the right




" """ vim-behaviour ------------------------------------------------------------

set shortmess=atI                       " shorten messages and don't show intro

set hidden                              " keep not currently displayed buffers in memory
set history=200                         " keep more commands in memory

set undolevels=1000                     " 1000 undos
set updatecount=100                     " switch every 100 chars

set more                                " use Unix-style more prompt

"set autoread                            " watch for file changes
set noautowrite                         " don't automagically write on :next




" """ text formating options ---------------------------------------------------

"set mouse=a                             " use the mouse in the console

set linebreak

"set textwidth=80                        " predefine textwidth
set textwidth=0                         " prevent Vim from automatically inserting line breaks
set wrap                                " soft wrap long lines
set wrapmargin=0                        " only used if textwidth=0

set formatoptions=tcrql                 " t - autowrap to textwidth - DISABELED
                                        " c - autowrap comments to textwidth
                                        " r - autoinsert comment leader with <Enter>
                                        " q - allow formatting of comments with :gq
                                        " l - don't format already long lines


set nostartofline                       " Stable cursor position

set completeopt=menu,longest,preview    " more autocomplete <Ctrl>-P options




" """ tab completion -----------------------------------------------------------

set wildmenu                            " turn on wild menu (using <Tab> at the command line)
set wildmode=list,longest,full          " set wildmenu to list choice

set complete=.,w,b,u,U,t,i,d            " do lots of scanning on tab completion

" usage of autocompletion
"   Ctrl-n for autocompletion
"   Ctrl-x Ctrl-f for file autocompletion



" """ define fold-method for vim-files -----------------------------------------

set foldenable                          " fold by default
set foldcolumn=5                        " see the width of the column to display folds on the left
set foldmethod=marker                   " fold based on markers ( by default {{{ .... }}} )




" tags and tag jumping ---------------------------------------------------------

" Create the `tags` file (may need to install ctags first, preferably 'universal-ctags')
command! MakeTags !ctags -R .

" usage of tags:
"   ^] to jump to tag under cursor
"   g^] for ambiguous tags
"   ^t to jump back up the tag stack
" but this doesn't help if you want a visual list of tags



" """ whitespace ---------------------------------------------------------------

set expandtab                           " expand tab to insert spaces instead of tabstops
set smarttab                            " when using tabstops add as many spaces until reaching a multiple of shiftwidth

set tabstop=4                           " tab results in 4 spaces
set softtabstop=4                       " set virtual tab stop (compat for 4-wide tabs)
set shiftwidth=2                        " define number of spaces that make a tabstop

set backspace=2                         " equivalent to: 'set backspace=indent,eol,start'

set autoindent                          " automatic indentation
set smartindent                         " try to be smart about indenting (C-style)
"set autoindent smartindent              " auto/smart indent

set shiftround                          " always round indents to multiple of shiftwidth
set copyindent                          " use existing indents for new indents
set preserveindent                      " save as much indent structure as possible



" show invisible characters and define invisible characters
set list                                " show invisible characters
"set nolist                              " necessary for wrapping
" list-option has to be disabled for line wrapping to work
set listchars=space:·,trail:°,tab:¤\ ,extends:»,precedes:«,nbsp:¬,eol:§

set showbreak=±\ 



" """ search setup -------------------------------------------------------------

set incsearch                           " Incremental searching
set hlsearch                            " Highlighting while searching
"set nohlsearch                          " turn off highlighting for searched expressions

set diffopt=filler,iwhite               " ignore all whitespace and sync

set ignorecase                          " ignore case when searching
set smartcase                           " when searching for lower case, also upper case is found
                                        " when searching for upper case, only upper case is found

"let loaded_matchparen=1                 " disable matched parenthesis plugin
set showmatch                           " show matching pairs of bracktes
set matchtime=2                         " blink matching chars for .x seconds

set path+=**                            " use a fuzzy search when searching for files



" remappings for the terminal -------------------------------------------------

if has("nvim")
  " use Alt-[ to exit terminal-mode
  tnoremap <A-[> <C-\><C-n>
  " use Alt-h/j/k/l to change splits in terminal mode
  tnoremap <A-h> <C-\><C-n><C-w>h
  tnoremap <A-j> <C-\><C-n><C-w>j
  tnoremap <A-k> <C-\><C-n><C-w>k
  tnoremap <A-l> <C-\><C-n><C-w>l
  " use Blt-h/j/k/l to changes splits in normal mode
  nnoremap <A-h> <C-w>h
  nnoremap <A-j> <C-w>j
  nnoremap <A-k> <C-w>k
  nnoremap <A-l> <C-w>l
endif


" other remappings ------------------------------------------------------------

nmap <leader>t :set list! <CR>

